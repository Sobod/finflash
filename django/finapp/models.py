from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here.


class Company(models.Model):
    company_name = models.CharField(max_length=50, primary_key=True, unique=True)
    company_symbol = models.CharField(max_length=40, null=False, blank=False)
    exchange_name = models.CharField(max_length=40, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=10000, blank=True)


class NiftyCompanies(models.Model):
    company_name = models.CharField(max_length=50, primary_key=True, unique=True)
    moneycontrol_link = models.CharField(max_length=1000, null=True, blank=True)
    screener_link = models.CharField(max_length=1000, null=True, blank=True)
    simply_wlst_link = models.CharField(max_length=1000, null=True, blank=True)
    company_symbol = models.CharField(max_length=40, null=False, blank=False)
    sector = models.CharField(max_length=100, null=False, blank=False)
    nifty_category = models.CharField(max_length=40, null=False, blank=False)
    my_portfolio = models.BooleanField(default=False)


class StockDaily(models.Model):
    id = models.AutoField(primary_key=True)
    company_name = models.ForeignKey(Company, on_delete=models.CASCADE)
    company_symbol = models.CharField(max_length=40, default='', null=False, blank=False)
    stock_data = JSONField(null=True)
    # for_date = models.DateField(null=False)
    # open = models.CharField(max_length=50, null=False, blank=True)
    # high = models.CharField(max_length=50, null=False, blank=True)
    # low = models.CharField(max_length=50, null=False, blank=True)
    # close = models.CharField(max_length=50, null=False, blank=True)
    # volume = models.CharField(max_length=50, null=False, blank=True)


class StockIntraDay(models.Model):
    id = models.AutoField(primary_key=True)
    company_name = models.ForeignKey(Company, on_delete=models.CASCADE)
    company_symbol = models.CharField(max_length=40, default='', null=False, blank=False)
    for_datetime = models.DateTimeField(null=False)
    interval_min = models.CharField(max_length=10, null=False, blank=False)
    stock_data = JSONField(null=True)
    # open = models.CharField(max_length=50, null=False, blank=True)
    # high = models.CharField(max_length=50, null=False, blank=True)
    # low = models.CharField(max_length=50, null=False, blank=True)
    # close = models.CharField(max_length=50, null=False, blank=True)
    # volume = models.CharField(max_length=50, null=False, blank=True)
