"""
news views file
"""
from time import sleep
import random

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.shortcuts import render

from finapp.models import NiftyCompanies
from finapp.serializers import NiftyCompaniesSerializer


def market_view(request):
    print('Building indices')
    all_companies = NiftyCompanies.objects.values_list('company_name').order_by('company_name')
    len_all_companies = NiftyCompanies.objects.count()
    no_of_columns = int(str(len_all_companies / 5).split('.')[0])
    no_of_rows = int(str(len_all_companies / no_of_columns).split('.')[0])
    company_moneycontrol_dict = {}
    if 'sector' in request.GET.keys():
        sector = str(request.GET['sector'])
        company_list = list(NiftyCompanies.objects.values('company_name', 'moneycontrol_link').filter(sector__icontains=sector, ).order_by('company_name'))
        company_moneycontrol_dict = moenycontrol_data(company_list)
    if 'nifty_category' in request.GET.keys():
        nifty_category = str(request.GET['nifty_category'])
        company_list = list(NiftyCompanies.objects.values('company_name', 'moneycontrol_link').filter(nifty_category=nifty_category, ).order_by('company_name'))
        company_moneycontrol_dict = moenycontrol_data(company_list)
    print(len(company_moneycontrol_dict))
    return render(request, 'market_view.html', {
        'company_list': all_companies,
        'rows': no_of_rows,
        'columns': no_of_columns,
        'moneycontrol_data': company_moneycontrol_dict
    })


def detailed_analysis(request):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(chrome_options=options)
    try:
        company_name = str(request.GET['search_company'])
        print('Getting analysis about {}'.format(company_name))
        company_from_db = NiftyCompanies.objects.values('simply_wlst_link').filter(company_name__icontains=company_name)
        if len(company_from_db) > 0:
            print("company available in db")
            simplywallst_link = company_from_db[0]['simply_wlst_link']
            print(str(simplywallst_link))
        else:
            driver.get('https:www.google.com')
            search_query = driver.find_element_by_name('q')
            search_string = str(company_name) + ' simply wall st'
            search_query.send_keys(search_string)
            sleep(random.random())
            search_query.send_keys(Keys.RETURN)
            sleep(3)
            source = driver.page_source
            raw = BeautifulSoup(source, features="lxml")
            sleep(random.randint(3, 5))
            serach_results_div = raw.find('div', {"class": "r"})
            simplywallst_link = ''
            for result in serach_results_div:
                if result.name == 'a':
                    simplywallst_link = result['href']
                    print('again:' + str(simplywallst_link))
        driver.get(simplywallst_link)
        sleep(random.randint(3, 5))
        source = driver.page_source
        raw_page = BeautifulSoup(source, features="lxml")
        company_name = raw_page.find('h1', {"data-cy-id": "company-header-title"}).text
        summary = raw_page.find('p', {"data-cy-id": "company-summary-desc"}).text
        rewards = ''
        risks = ''
        try:
            reward_index = 0
            risk_index = 0
            risk_rewards = raw_page.find('div', {"data-cy-id": "risk-reward-wrapper"})
            risk_rewards_list = list()
            for riskreward in risk_rewards:
                if riskreward.name == 'h4' or riskreward.name == 'blockquote':
                    risk_rewards_list.append(riskreward.text)
            for item in risk_rewards_list:
                if item == 'Rewards':
                    reward_index = risk_rewards_list.index(item)
                if item == 'Risk Analysis':
                    risk_index = risk_rewards_list.index(item)
            rewards = risk_rewards_list[reward_index + 1:risk_index]
            risks = risk_rewards_list[risk_index + 1:]
        except Exception as e:
            print({"Error": e})
        all_insights = raw_page.findAll('p')
        insights_dict = list()
        for insight in all_insights:
            tmp_insight_dict = {}
            if ':' in insight.text and insight.find('span') is not None:
                tmp_insight_dict['insight_name'] = insight.text.split(':')[0]
                tmp_insight_dict['insight_data'] = insight.text.split(':')[1]
                if insight.find('span').get('value') == 'Pass':
                    tmp_insight_dict['insight_color'] = '#2BD784'
                else:
                    tmp_insight_dict['insight_color'] = '#E64141'
            if len(tmp_insight_dict) > 0:
                insights_dict.append(tmp_insight_dict)
        print(insights_dict)
    except Exception as e:
        company_name = ''
        rewards = ''
        risks = ''
        insights_dict = {}
        print("Exception is: {}".format(e))
    finally:
        driver.quit()
    print(rewards, risks)
    return render(request, 'detailed_analysis.html', {
        "company_name": company_name.title(),
        # "stock_news_dict": stock_news_list,
        "insights_dict": insights_dict,
        "rewards": rewards,
        "risks": risks
    })


def moenycontrol_data(company_list):
    print('Getting money control data')
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(chrome_options=options)
    all_data = []
    company_data = {}
    try:
        for company in company_list:
            search_url = company['moneycontrol_link']
            driver.get(search_url)
            source = driver.page_source
            rawsearch = BeautifulSoup(source, features="lxml")
            company_data = {'company_name': company['company_name']}
            market_depth_buy = rawsearch.find('div', {"class": "buy_txt price_marketdepth_buy"})
            if market_depth_buy is not None:
                company_data['market_depth_buy'] = market_depth_buy.text.split('BUY')[1].strip()
            market_depth_sell = rawsearch.find('div', {"class": "sell_txt price_marketdepth_sell"})
            if market_depth_sell is not None:
                company_data['market_depth_sell'] = market_depth_sell.text.split('SELL')[1].strip()
            company_sentiment = rawsearch.find('ul', {"class": "buy_sellper"})
            if company_sentiment is not None:
                for sentiment in company_sentiment:
                    if sentiment.name == 'li':
                        company_data[sentiment.text.split(' ')[1]] = sentiment.text.split(' ')[0]
            stock_exchanges = rawsearch.findAll('div', {"class": "pcnsb div_live_price_wrap"})
            exchange_count = 0
            price_count = 0
            for tag in stock_exchanges:
                exchange_count += 1
                for price in tag:
                    price_count += 1
                    if price.name == 'span' and price.text != '' and exchange_count == 2:
                        if company_data.get('current_price') is None:
                            company_data['current_price'] = price.text
                        else:
                            company_data['price_change'] = price.text.split(' ')[0]
            standalone_div = rawsearch.find('div', {"id": "standalone_valuation"})
            headings = standalone_div.findAll('div', {"class": "value_txtfl"})
            values = standalone_div.findAll('div', {"class": "value_txtfr"})
            headings_list = []
            values_list = []
            for i in headings:
                headings_list.append(str(i.text.split("(")[0]).replace(' ', '').replace('.', '').replace('/', ''))
            for i in values:
                values_list.append(i.text)
            market_dict = dict(zip(headings_list, values_list))
            company_data.update(market_dict)
            try:
                if float(company_data['PE']) < float(company_data['IndustryPE']):
                    company_data['pe_color'] = '#2BD784'
                    # company_data['industry_pe_color'] = '#E64141'
                elif float(company_data['PE']) > float(company_data['IndustryPE']):
                    company_data['pe_color'] = '#E64141'
                    # company_data['industry_pe_color'] = '#2BD784'
                else:
                    company_data['PE_color'] = '#7B8084'
                    # company_data['industry_pe_color'] = '#7B8084'
            except Exception as e:
                print({"Error": e})
                company_data['PE_color'] = '#E64141'
                # company_data['industry_pe_color'] = '#2BD784'
            try:
                if float(company_data['PriceBook']) < 1:
                    company_data['price_book_color'] = '#2BD784'
                elif float(company_data['PriceBook']) > 0:
                    company_data['price_book_color'] = '#E64141'
                else:
                    company_data['price_book_color'] = '#7B8084'
            except Exception as e:
                print({"Error": e})
                company_data['price_book_color'] = '#E64141'
            try:
                if float(company_data['DividendYield']) > float(2.5):
                    company_data['DividendYield_color'] = '#2BD784'
                elif float(company_data['DividendYield']) < float(2.5):
                    company_data['DividendYield_color'] = '#E64141'
                else:
                    company_data['DividendYield_color'] = '#7B8084'
            except Exception as e:
                print({"Error": e})
                company_data['PE_color'] = '#E64141'
            if float(company_data['price_change']) > 0:
                company_data['color'] = '#2BD784'
            elif float(company_data['price_change']) < 0:
                company_data['color'] = '#E64141'
            else:
                company_data['color'] = '#7B8084'
            company_data['href'] = '/detailed_analysis?search_company=' + company['company_name']
            all_data.append(company_data)
        # print(all_data)
    except Exception as error:
        print({'Error': error})
    finally:
        driver.quit()
    return all_data
