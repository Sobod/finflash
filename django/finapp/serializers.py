"""
serializers file for apis
"""
import pytz
from rest_framework import serializers

from finapp.models import Company, NiftyCompanies, StockDaily, StockIntraDay


class CompanySerializer(serializers.ModelSerializer):
    """
    Company serializer class
    """

    exchange_name_choices = [('NSE', 'NSE'), ('BSE', 'BSE')]

    exchange_name = serializers.ChoiceField(choices=exchange_name_choices, required=False)

    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = Company
        fields = (
            'company_name',
            'company_symbol',
            'created',
            'exchange_name',
            'description',
        )


class NiftyCompaniesSerializer(serializers.ModelSerializer):
    """
    Company serializer class
    """

    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = Company
        fields = (
            'company_name',
            'moneycontrol_link',
            'screener_link',
            'simply_wlst_link',
            'company_symbol',
            'sector',
            'nifty_category',
            'my_portfolio',
        )


class StockDailySerializer(serializers.ModelSerializer):
    """
    StockDaily serializer class
    """
    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = StockDaily
        fields = (
            'company_name',
            'company_symbol',
            'stock_data'
            # 'for_date',
            # 'open',
            # 'high',
            # 'low',
            # 'close',
            # 'volume',
        )


class StockDailySerializerPost(serializers.ModelSerializer):
    """
    StockDaily serializer class
    """

    exchange_choices = [('NSE', 'NSE'), ('BSE', 'BSE')]

    exchange = serializers.ChoiceField(choices=exchange_choices, required=False)

    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = StockDaily
        fields = (
            'company_name',
            'exchange',
            'company_symbol',
        )


class StockIntraDaySerializer(serializers.ModelSerializer):
    """
    StockIntraDay1 serializer class
    """
    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = StockIntraDay
        fields = (
            'id',
            'company_name',
            'company_symbol',
            'for_datetime',
            'interval_min',
            'stock_data',
            # 'open',
            # 'high',
            # 'low',
            # 'close',
            # 'volume',
        )


class StockIntraDaySerializerPost(serializers.ModelSerializer):
    """
    StockIntraDay serializer class
    """

    exchange_choices = [('NSE', 'NSE'), ('BSE', 'BSE')]
    interval_min_choices = [('1min', '1min'), ('5min', '5min'), ('15min', '15min'), ('30min', '30min'), ('60min', '60min')]

    exchange = serializers.ChoiceField(choices=exchange_choices, required=False)
    interval_min = serializers.ChoiceField(choices=interval_min_choices, required=False)

    class Meta:  # pylint: disable=missing-class-docstring, too-few-public-methods
        model = StockIntraDay
        fields = (
            'company_name',
            'exchange',
            'company_symbol',
            'interval_min'
        )
