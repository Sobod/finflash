"""
urls file for apis
"""
from django.urls import path, include
from rest_framework import routers

from finapp import views

router = routers.DefaultRouter()
router.register(r'company', views.CompanyViewSet)
router.register(r'stockDaily', views.StockDailyViewSet)
router.register(r'stockIntraDay', views.StockIntraDayViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
