"""
utils file for finapp
"""
import os
from datetime import datetime
import random
from pathlib import Path
from pytz import timezone


from dotenv import load_dotenv

from alpha_vantage.timeseries import TimeSeries

from finapp.models import Company, StockDaily, StockIntraDay

env_path = Path('../..') / '.env'
load_dotenv(dotenv_path=env_path)

API_KEY = os.getenv('API_KEY')
DUPLICATE_API_KEY = os.getenv('DUPLICATE_API_KEY')


def est_to_ist(datetime_string):
    IST = timezone('Asia/Kolkata')
    datetime_object = datetime.strptime(datetime_string, "%Y-%m-%d %H:%M:%S")
    datetime_object_EST = timezone('US/Eastern').localize(datetime_object)
    return datetime_object_EST.astimezone(IST)


def compute_highchart_series_daily(request):
    """
    A function to compute series data for a given table
    """
    table_data = StockDaily.objects.values('id', 'company_symbol', 'stock_data')
    if len(table_data) > 0:
        color_codes = ['#8e5ea2', '#c45850', '#3e95cd', '#3cba9f', '#9A6324', '#808000', '#000075']
        table_data = list(table_data)
        date_data = list()
        series = list()
        count = 0
        for company_dict in table_data:
            complete_data = list()
            company_symbol = company_dict['company_symbol']
            company_stock_data = company_dict['stock_data']
            sorted_keys_stock_data = sorted((company_stock_data.keys()))
            for key in sorted_keys_stock_data:
                complete_data.append([str(key), float((company_dict['stock_data'])[key]['1. open'])])
            series_item = {
                'name': company_symbol,
                'data': complete_data,
                'color': color_codes[count]
            }
            series.append(series_item)
            count += 1
    else:
        series = []
    return series


def compute_highchart_series_intraday(request):
    """
    A function to compute series data for a given table
    """
    table_data = StockIntraDay.objects.values('id', 'company_symbol', 'stock_data')
    if len(table_data) > 0:
        color_codes = ['#8e5ea2', '#c45850', '#3e95cd', '#3cba9f', '#9A6324', '#808000', '#000075']
        table_data = list(table_data)
        date_data = list()
        series = list()
        count = 0
        for company_dict in table_data:
            complete_data = list()
            company_symbol = company_dict['company_symbol']
            company_stock_data = company_dict['stock_data']
            sorted_keys_stock_data = sorted((company_stock_data.keys()))
            for key in sorted_keys_stock_data:
                complete_data.append([str(key), float((company_dict['stock_data'])[key]['1. open'])])
            series_item = {
                'name': company_symbol,
                'data': complete_data,
                'color': color_codes[count]
            }
            series.append(series_item)
            count += 1
    else:
        series = []
    return series


def adhoc_compute_highchart_series_intraday(request):
    """
    A function to compute series data for a given table
    """
    company_name = str(request.GET['company_name'])
    interval = str(request.GET['interval'])
    outputsize = str(request.GET['outputsize'])
    # print(str(request.GET.getlist('checks')))
    company_meta = Company.objects.values('exchange_name', 'company_symbol').filter(company_name=company_name).get()
    symbol = str(company_meta['exchange_name']) + ':' + str(company_meta['company_symbol'])
    company_stock_data, meta = TimeSeries(API_KEY, output_format='json').get_intraday(
        symbol=symbol, interval=interval, outputsize=outputsize
    )
    company_stock_data_pandas, meta_pandas = TimeSeries(API_KEY, output_format='pandas').get_intraday(
        symbol=symbol, interval=interval, outputsize=outputsize
    )
    company_stock_data_pandas.columns = ['open', 'high', 'low', 'close', 'volume']
    company_stock_data_pandas.to_csv('/home/jeanmartin.com/sobod.p/finflash/django/finapp/c.csv')
    # company_stock_data = {"2019-11-05 03:15:00": {"3. low": "379.0000", "1. open": "379.0000", "2. high": "379.5500", "4. close": "379.1500", "5. volume": "46"}, "2019-11-05 03:16:00": {"3. low": "379.2500", "1. open": "379.3500", "2. high": "379.3500", "4. close": "379.3000", "5. volume": "68"}, "2019-11-05 03:17:00": {"3. low": "379.3000", "1. open": "379.3000", "2. high": "379.3000", "4. close": "379.3000", "5. volume": "29"}, "2019-11-05 03:18:00": {"3. low": "379.3000", "1. open": "379.3000", "2. high": "379.3000", "4. close": "379.3000", "5. volume": "47"}, "2019-11-05 03:19:00": {"3. low": "379.0500", "1. open": "379.2000", "2. high": "379.2000", "4. close": "379.0500", "5. volume": "383"}, "2019-11-05 03:20:00": {"3. low": "379.0000", "1. open": "379.2500", "2. high": "379.2500", "4. close": "379.2000", "5. volume": "576"}, "2019-11-05 03:21:00": {"3. low": "379.0000", "1. open": "379.2500", "2. high": "379.2500", "4. close": "379.2500", "5. volume": "281"}, "2019-11-05 03:22:00": {"3. low": "379.2500", "1. open": "379.2500", "2. high": "379.6500", "4. close": "379.3000", "5. volume": "153"}, "2019-11-05 03:23:00": {"3. low": "379.2500", "1. open": "379.2500", "2. high": "379.3000", "4. close": "379.2500", "5. volume": "264"}, "2019-11-05 03:24:00": {"3. low": "378.0500", "1. open": "379.0000", "2. high": "379.0000", "4. close": "378.0500", "5. volume": "822"}, "2019-11-05 03:25:00": {"3. low": "378.0500", "1. open": "378.0500", "2. high": "378.8500", "4. close": "378.8500", "5. volume": "0"}, "2019-11-05 03:26:00": {"3. low": "378.2000", "1. open": "378.2000", "2. high": "378.2000", "4. close": "378.2000", "5. volume": "57"}, "2019-11-05 03:27:00": {"3. low": "378.3500", "1. open": "378.9000", "2. high": "378.9000", "4. close": "378.3500", "5. volume": "83"}, "2019-11-05 03:28:00": {"3. low": "378.7000", "1. open": "379.0000", "2. high": "379.2500", "4. close": "378.7500", "5. volume": "169"}, "2019-11-05 03:29:00": {"3. low": "378.1000", "1. open": "378.7500", "2. high": "378.7500", "4. close": "378.2500", "5. volume": "138"}, "2019-11-05 03:30:00": {"3. low": "377.3000", "1. open": "378.0000", "2. high": "378.2000", "4. close": "377.3000", "5. volume": "496"}, "2019-11-05 03:31:00": {"3. low": "377.5000", "1. open": "377.5000", "2. high": "377.5000", "4. close": "377.5000", "5. volume": "0"}, "2019-11-05 03:32:00": {"3. low": "376.9122", "1. open": "377.9658", "2. high": "378.3636", "4. close": "378.0158", "5. volume": "1169"}, "2019-11-05 03:33:00": {"3. low": "377.2500", "1. open": "377.7000", "2. high": "377.9000", "4. close": "377.3500", "5. volume": "499"}, "2019-11-05 03:34:00": {"3. low": "377.8000", "1. open": "377.8000", "2. high": "378.0000", "4. close": "378.0000", "5. volume": "67"}, "2019-11-05 03:35:00": {"3. low": "376.0500", "1. open": "378.0000", "2. high": "378.0000", "4. close": "376.0500", "5. volume": "756"}, "2019-11-05 03:36:00": {"3. low": "375.8000", "1. open": "376.0500", "2. high": "376.0500", "4. close": "375.8000", "5. volume": "63"}, "2019-11-05 03:37:00": {"3. low": "375.2500", "1. open": "375.8000", "2. high": "375.8000", "4. close": "375.3000", "5. volume": "901"}, "2019-11-05 03:38:00": {"3. low": "375.2513", "1. open": "375.7140", "2. high": "376.8528", "4. close": "376.5140", "5. volume": "1188"}, "2019-11-05 03:39:00": {"3. low": "376.5495", "1. open": "376.5636", "2. high": "376.9999", "4. close": "376.6636", "5. volume": "15"}, "2019-11-05 03:40:00": {"3. low": "376.4500", "1. open": "376.5000", "2. high": "376.6500", "4. close": "376.5000", "5. volume": "128"}, "2019-11-05 03:41:00": {"3. low": "376.5000", "1. open": "376.5000", "2. high": "376.5000", "4. close": "376.5000", "5. volume": "28"}, "2019-11-05 03:42:00": {"3. low": "375.6595", "1. open": "376.4651", "2. high": "376.8607", "4. close": "375.8151", "5. volume": "273"}, "2019-11-05 03:43:00": {"3. low": "375.8000", "1. open": "375.8000", "2. high": "376.4500", "4. close": "376.4500", "5. volume": "42"}, "2019-11-05 03:44:00": {"3. low": "376.1800", "1. open": "376.5100", "2. high": "378.1800", "4. close": "376.2100", "5. volume": "236"}, "2019-11-05 03:45:00": {"3. low": "375.8500", "1. open": "375.8500", "2. high": "375.8500", "4. close": "375.8500", "5. volume": "4"}, "2019-11-05 03:46:00": {"3. low": "376.2000", "1. open": "376.2000", "2. high": "376.6000", "4. close": "376.6000", "5. volume": "57"}, "2019-11-05 03:47:00": {"3. low": "376.6000", "1. open": "376.6000", "2. high": "376.6000", "4. close": "376.6000", "5. volume": "19"}, "2019-11-05 03:48:00": {"3. low": "376.4500", "1. open": "376.6000", "2. high": "376.6000", "4. close": "376.4500", "5. volume": "117"}, "2019-11-05 03:49:00": {"3. low": "376.1000", "1. open": "376.6000", "2. high": "376.6000", "4. close": "376.1000", "5. volume": "114"}, "2019-11-05 03:50:00": {"3. low": "376.0500", "1. open": "376.1000", "2. high": "376.5000", "4. close": "376.5000", "5. volume": "456"}, "2019-11-05 03:51:00": {"3. low": "376.4000", "1. open": "377.1500", "2. high": "377.1500", "4. close": "376.4000", "5. volume": "189"}, "2019-11-05 03:52:00": {"3. low": "375.5000", "1. open": "376.0500", "2. high": "376.0500", "4. close": "375.5000", "5. volume": "251"}, "2019-11-05 03:53:00": {"3. low": "375.5000", "1. open": "375.6000", "2. high": "375.6500", "4. close": "375.6500", "5. volume": "92"}, "2019-11-05 03:54:00": {"3. low": "375.9500", "1. open": "375.9500", "2. high": "375.9500", "4. close": "375.9500", "5. volume": "33"}, "2019-11-05 03:55:00": {"3. low": "375.8000", "1. open": "375.8000", "2. high": "375.8000", "4. close": "375.8000", "5. volume": "5"}, "2019-11-05 03:56:00": {"3. low": "375.2500", "1. open": "375.6500", "2. high": "375.6500", "4. close": "375.2500", "5. volume": "313"}, "2019-11-05 03:57:00": {"3. low": "375.5500", "1. open": "375.5500", "2. high": "375.5500", "4. close": "375.5500", "5. volume": "2"}, "2019-11-05 03:58:00": {"3. low": "375.5500", "1. open": "375.5500", "2. high": "375.7000", "4. close": "375.7000", "5. volume": "129"}, "2019-11-05 03:59:00": {"3. low": "375.7000", "1. open": "375.7000", "2. high": "376.0000", "4. close": "375.7000", "5. volume": "503"}, "2019-11-05 04:00:00": {"3. low": "375.8500", "1. open": "376.0000", "2. high": "376.0000", "4. close": "375.8500", "5. volume": "192"}, "2019-11-05 04:01:00": {"3. low": "375.7000", "1. open": "376.0000", "2. high": "376.0000", "4. close": "375.7000", "5. volume": "125"}, "2019-11-05 04:02:00": {"3. low": "375.3500", "1. open": "375.7000", "2. high": "376.0000", "4. close": "375.3500", "5. volume": "856"}, "2019-11-05 04:03:00": {"3. low": "375.2500", "1. open": "375.9500", "2. high": "375.9500", "4. close": "375.2500", "5. volume": "79"}, "2019-11-05 04:05:00": {"3. low": "375.2500", "1. open": "375.2500", "2. high": "375.2500", "4. close": "375.2500", "5. volume": "29"}, "2019-11-05 04:07:00": {"3. low": "375.4500", "1. open": "375.4500", "2. high": "375.4500", "4. close": "375.4500", "5. volume": "9"}, "2019-11-05 04:08:00": {"3. low": "375.2500", "1. open": "375.4500", "2. high": "375.8500", "4. close": "375.5500", "5. volume": "63"}, "2019-11-05 04:09:00": {"3. low": "375.1950", "1. open": "375.5574", "2. high": "375.8456", "4. close": "375.5074", "5. volume": "735"}, "2019-11-05 04:10:00": {"3. low": "375.5500", "1. open": "375.5500", "2. high": "376.0000", "4. close": "376.0000", "5. volume": "394"}, "2019-11-05 04:11:00": {"3. low": "375.5500", "1. open": "375.5500", "2. high": "375.5500", "4. close": "375.5500", "5. volume": "28"}, "2019-11-05 04:12:00": {"3. low": "375.1000", "1. open": "375.5000", "2. high": "375.5000", "4. close": "375.1000", "5. volume": "415"}, "2019-11-05 04:14:00": {"3. low": "375.7500", "1. open": "375.7500", "2. high": "375.7500", "4. close": "375.7500", "5. volume": "22"}, "2019-11-05 04:15:00": {"3. low": "375.2500", "1. open": "375.7500", "2. high": "376.3500", "4. close": "375.2500", "5. volume": "1418"}, "2019-11-05 04:16:00": {"3. low": "375.7000", "1. open": "375.7000", "2. high": "375.7000", "4. close": "375.7000", "5. volume": "0"}, "2019-11-05 04:17:00": {"3. low": "374.7000", "1. open": "375.2500", "2. high": "375.6500", "4. close": "374.7000", "5. volume": "402"}, "2019-11-05 04:18:00": {"3. low": "374.7000", "1. open": "374.7500", "2. high": "375.0000", "4. close": "375.0000", "5. volume": "152"}, "2019-11-05 04:20:00": {"3. low": "375.0000", "1. open": "375.0000", "2. high": "375.0000", "4. close": "375.0000", "5. volume": "268"}, "2019-11-05 04:21:00": {"3. low": "374.8000", "1. open": "375.0000", "2. high": "375.0000", "4. close": "374.9500", "5. volume": "8"}, "2019-11-05 04:22:00": {"3. low": "374.8603", "1. open": "375.0157", "2. high": "375.3607", "4. close": "374.9157", "5. volume": "771"}, "2019-11-05 04:23:00": {"3. low": "374.5500", "1. open": "374.5500", "2. high": "374.5500", "4. close": "374.5500", "5. volume": "47"}, "2019-11-05 04:24:00": {"3. low": "374.5500", "1. open": "374.5500", "2. high": "375.0000", "4. close": "375.0000", "5. volume": "49"}, "2019-11-05 04:25:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.5000", "4. close": "374.5000", "5. volume": "11"}, "2019-11-05 04:26:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.5000", "4. close": "374.5000", "5. volume": "6"}, "2019-11-05 04:27:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.7500", "4. close": "374.7500", "5. volume": "5"}, "2019-11-05 04:28:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.7000", "4. close": "374.5000", "5. volume": "207"}, "2019-11-05 04:29:00": {"3. low": "373.7000", "1. open": "374.0000", "2. high": "374.0000", "4. close": "373.7000", "5. volume": "255"}, "2019-11-05 04:30:00": {"3. low": "373.7000", "1. open": "373.7000", "2. high": "374.8000", "4. close": "374.7500", "5. volume": "175"}, "2019-11-05 04:31:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.9000", "4. close": "374.9000", "5. volume": "445"}, "2019-11-05 04:32:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.5500", "4. close": "374.5500", "5. volume": "136"}, "2019-11-05 04:33:00": {"3. low": "374.4500", "1. open": "374.4500", "2. high": "374.4500", "4. close": "374.4500", "5. volume": "20"}, "2019-11-05 04:34:00": {"3. low": "373.5500", "1. open": "374.7000", "2. high": "374.8000", "4. close": "373.8500", "5. volume": "637"}, "2019-11-05 04:35:00": {"3. low": "374.3624", "1. open": "374.4598", "2. high": "375.1632", "4. close": "374.8099", "5. volume": "193"}, "2019-11-05 04:36:00": {"3. low": "374.6446", "1. open": "374.7573", "2. high": "375.0449", "4. close": "374.7073", "5. volume": "0"}, "2019-11-05 04:38:00": {"3. low": "374.0000", "1. open": "374.3000", "2. high": "374.8000", "4. close": "374.6000", "5. volume": "514"}, "2019-11-05 04:39:00": {"3. low": "374.5500", "1. open": "374.9000", "2. high": "374.9000", "4. close": "374.6000", "5. volume": "153"}, "2019-11-05 04:40:00": {"3. low": "374.0000", "1. open": "374.6500", "2. high": "375.0000", "4. close": "374.0000", "5. volume": "296"}, "2019-11-05 04:41:00": {"3. low": "373.0000", "1. open": "374.0000", "2. high": "374.0000", "4. close": "373.0000", "5. volume": "479"}, "2019-11-05 04:42:00": {"3. low": "374.0000", "1. open": "374.1000", "2. high": "374.4500", "4. close": "374.0000", "5. volume": "142"}, "2019-11-05 04:43:00": {"3. low": "374.0000", "1. open": "374.4500", "2. high": "374.4500", "4. close": "374.0000", "5. volume": "529"}, "2019-11-05 04:44:00": {"3. low": "374.0000", "1. open": "374.4000", "2. high": "374.4000", "4. close": "374.2000", "5. volume": "16"}, "2019-11-05 04:45:00": {"3. low": "373.2936", "1. open": "374.3573", "2. high": "374.6446", "4. close": "373.3573", "5. volume": "1289"}, "2019-11-05 04:46:00": {"3. low": "373.0000", "1. open": "373.8500", "2. high": "373.8500", "4. close": "373.0000", "5. volume": "496"}, "2019-11-05 04:47:00": {"3. low": "373.0000", "1. open": "373.0000", "2. high": "373.5000", "4. close": "373.0000", "5. volume": "173"}, "2019-11-05 04:48:00": {"3. low": "373.4500", "1. open": "373.4500", "2. high": "373.9500", "4. close": "373.9500", "5. volume": "188"}, "2019-11-05 04:49:00": {"3. low": "373.7000", "1. open": "373.9500", "2. high": "374.0000", "4. close": "374.0000", "5. volume": "589"}, "2019-11-05 04:50:00": {"3. low": "374.0000", "1. open": "374.4000", "2. high": "374.9500", "4. close": "374.3000", "5. volume": "288"}, "2019-11-05 04:51:00": {"3. low": "374.0500", "1. open": "374.4000", "2. high": "374.4000", "4. close": "374.4000", "5. volume": "114"}, "2019-11-05 04:52:00": {"3. low": "374.2500", "1. open": "374.4000", "2. high": "374.9500", "4. close": "374.5000", "5. volume": "613"}, "2019-11-05 04:53:00": {"3. low": "374.5000", "1. open": "374.9500", "2. high": "375.0000", "4. close": "374.5000", "5. volume": "519"}, "2019-11-05 04:54:00": {"3. low": "374.5000", "1. open": "374.9500", "2. high": "374.9500", "4. close": "374.5500", "5. volume": "124"}, "2019-11-05 04:55:00": {"3. low": "374.4505", "1. open": "374.5640", "2. high": "375.3514", "4. close": "375.0140", "5. volume": "659"}, "2019-11-05 04:56:00": {"3. low": "374.5000", "1. open": "374.5000", "2. high": "374.9500", "4. close": "374.9500", "5. volume": "594"}, "2019-11-05 04:57:00": {"3. low": "374.8000", "1. open": "374.8000", "2. high": "375.7000", "4. close": "375.7000", "5. volume": "1717"}, "2019-11-05 04:58:00": {"3. low": "374.0636", "1. open": "375.7121", "2. high": "376.0656", "4. close": "375.1621", "5. volume": "950"}, "2019-11-05 04:59:00": {"3. low": "374.8000", "1. open": "375.7500", "2. high": "378.4500", "4. close": "374.8000", "5. volume": "2890"}}
    price_minimum = -10
    price_maximum = -10
    volume_minimum = -10
    volume_maximum = -10
    x_axis_list = list()
    if len(company_stock_data) > 0:
        from_data = str(request.GET['fromdate'])
        to_date = str(request.GET['todate'])
        color_codes = ['#8e5ea2', '#c45850', '#3e95cd', '#3cba9f', '#9A6324', '#808000', '#000075']
        price_series = list()
        open_data = list()
        close_data = list()
        high_data = list()
        low_data = list()
        volume_data = list()
        price_chart_data = list()
        price_chart = ['open', 'high', 'low', 'close']
        for plottype in price_chart:
            if plottype in request.GET.keys() and request.GET[plottype] == 'yes':
                dataname = plottype + '_data'
                price_chart_data.append(dataname)
        sorted_keys_stock_data = sorted((company_stock_data.keys()))
        for key in sorted_keys_stock_data:
            if from_data <= str(key).split(' ')[0] <= to_date:
                open_value = float(company_stock_data[key]['1. open'])
                high_value = float(company_stock_data[key]['2. high'])
                low_value = float(company_stock_data[key]['3. low'])
                close_value = float(company_stock_data[key]['4. close'])
                volume_value = int(company_stock_data[key]['5. volume'])
                x_date = str(est_to_ist(str(key))).split('+')[0]
                x_axis_list.append(x_date)
                open_data.append([x_date, open_value])
                close_data.append([x_date, close_value])
                high_data.append([x_date, high_value])
                low_data.append([x_date, low_value])
                volume_data.append([x_date, volume_value])
                if price_minimum < 0 or price_minimum > open_value:
                    price_minimum = open_value
                if price_maximum < 0 or price_maximum < open_value:
                    price_maximum = open_value
                if volume_minimum < 0 or volume_minimum > volume_value:
                    volume_minimum = volume_value
                if volume_maximum < 0 or volume_maximum < volume_value:
                    volume_maximum = volume_value
        data_dict = {'open_data': open_data, 'high_data': high_data, 'low_data': low_data, 'close_data': close_data}
        for i in range(0, len(price_chart_data)):
            series_item = {
                'name': str(price_chart_data[i]),
                'data': data_dict[price_chart_data[i]],
                'color': random.choice(color_codes)
            }
            price_series.append(series_item)
        volume_series = [{
            'name': str(company_name),
            'data': volume_data,
            'color': random.choice(color_codes)
        }]
    else:
        price_series = []
        volume_series = []
    return price_series, symbol, x_axis_list, price_minimum, price_maximum, volume_series, volume_minimum, volume_maximum
