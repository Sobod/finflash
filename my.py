import requests
from collections import defaultdict
from time import sleep
import random
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager


def generate_links(com_list):
    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')
    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    try:
        final_link = ''
        for i in com_list:
            search_list = ['moneycontrol', 'screener', 'simply wall st']
            var = defaultdict(lambda: 0)
            for k in search_list:
                print('Getting {} link for {}'.format(k, i))
                driver.get('https://www.google.co.in/')
                search_query = driver.find_element_by_name('q')
                search_string = str(i) + ' ' + k
                search_query.send_keys(search_string)
                sleep(random.random())
                search_query.send_keys(Keys.RETURN)
                source = driver.page_source
                raw = BeautifulSoup(source, features="lxml")
                serach_results_div = raw.find('div', {"class": "r"})
                for result in serach_results_div:
                    if result.name == 'a':
                        var[k] = result['href']
                        print('again:' + str(var[k]))
                        if 'moneycontrol' in var[k]:
                            driver.get(var[k])
                            source = driver.page_source
                            raw = BeautifulSoup(source, features="lxml")
                            var['company_name'] = raw.find('h1', {"class": "pcstname"}).text
                            var['company_symbol'] = raw.find('ctag', {"class": "mob-hide"}).text.split('NSE:')[1].replace('|', '').strip()
                            var['sector'] = raw.find('span', {"class": "hidden-lg"}).text
            init_text = "INSERT INTO public.finapp_niftycompanies (company_name, moneycontrol_link, screener_link, simply_wlst_link, company_symbol, sector, nifty_category, my_portfolio) VALUES( {}, {}, {}, {}, {}, {}, 'Nifty 50', 'false');".format(
                    "'" + str(var['company_name']) + "'",
                    "'" + str(var['moneycontrol']) + "'",
                    "'" + str(var['screener']) + "'",
                    "'" + str(var['simply wall st']) + "'",
                    "'" + str(var['company_symbol']) + "'",
                    "'" + str(var['sector']) + "'",
                )
            print('################')
            print('\n')
            print(init_text)
            print('\n')
            print('################')
    finally:
        driver.close()
    return 'DONE'


print(generate_links(
    [
        'ZEE ENTERTAINMENT',
        'TV18 BROADCAST',
        'SUN TV',
        'SAREGAMA',
        'PVR LTD',
        'INOX LEISURE',
        'DISH TV',
     ]))
# print(generate_links(['Bank of Baroda']))
